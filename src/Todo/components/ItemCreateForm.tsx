import React, { ChangeEvent, FormEvent } from 'react';

interface ItemCreateProps {
  addItem: (content: string) => void
}

const ItemCreateForm = ({ addItem }: ItemCreateProps) => {

  const [content, setContent] = React.useState<string>("");

  const handleSubmit = (e: FormEvent) => {
    e.preventDefault();

    if (content.length === 0) {
      return;
    }

    addItem(content);
    setContent("");
  };

  const changeContent = (e: ChangeEvent<HTMLInputElement>) => {
    e.preventDefault();
    setContent(e.target.value);
  };

  return (
    <form onSubmit={handleSubmit}>
      <div className="row">
        <div className="col col-12 col-md-9 py-1">
          <input className="form-control float-left itemField" type="text" value={content} onChange={changeContent} />
        </div>
        <div className="col col-12 col-md-3 py-1">
          <input className="btn btn-primary btn-block" type="submit" value="Add item" />
        </div>
      </div>
    </form>
  );

}

export default ItemCreateForm;
