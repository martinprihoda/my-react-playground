import React, { useState } from 'react';
import Item from '../interfaces/Item';
import ItemDetail from './ItemDetail';
import ItemCreateForm from './ItemCreateForm';

const ItemList = () => {

  const [items, setItems] = useState<Array<Item>>([]);

  const addItem = (content: string) => {
    const item: Item = {
      id: generateId(),
      content: content, 
      complete: false
    }

    setItems([...items, item]);
  };

  const removeItem = (id: number) => {
    let newItems = [...items];

    newItems = newItems.filter((item: Item) => {
      return item.id !== id;
    });

    setItems(newItems);
  };

  const completeItem = (id: number) => {
    let newItems = [...items];
    let item = newItems.find((item: Item) => item.id === id);

    if (item) {
      item.complete = true;
    }

    setItems(newItems);
  };

  function generateId(): number {
    return items.length > 0 
      ? items[items.length - 1].id + 1 
      : 0;
  }

  return (
    <div className="container">
      <div className="card">
        <ul className="list-group list-group-flush">
          {items.map(item => (
            <li className="list-group-item" key={item.id}>
              <ItemDetail 
                item={item}
                removeItem={removeItem}
                completeItem={completeItem}
              />
            </li>
          ))}
        </ul>
        <div className="card-footer">
          <ItemCreateForm addItem={addItem} />
        </div>
      </div>
    </div>
  );
}

export default ItemList;
