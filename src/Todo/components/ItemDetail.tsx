import React from 'react';
import Item from '../interfaces/Item';

interface ItemDetailProps {
  item: Item,
  removeItem: (index: number) => void, 
  completeItem: (index: number) => void
}

const ItemDetail = ({ item, removeItem, completeItem}: ItemDetailProps) => {

  function handleRemoveItem() {
    console.log('removing');
    removeItem(item.id);
  }

  function handleCompleteItem() {
    completeItem(item.id);
  }
  
  return (
    <div className="Item">
      {item.complete ? <s>{item.content}</s> : item.content}
      <button className="btn btn-danger float-right mx-1" onClick={handleRemoveItem}>
        Remove
      </button>
      {!item.complete && (
        <button className="btn btn-primary float-right mx-1" onClick={handleCompleteItem}>
          Complete
        </button>
      )}
    </div>
  );

}

export default ItemDetail;
