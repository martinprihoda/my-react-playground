export default interface Item {
    id: number;
    content: string;
    complete: boolean;
}