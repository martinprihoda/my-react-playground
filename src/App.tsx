import React from 'react';
import ItemList from './Todo/components/ItemList';

function App() {
  return (
    <ItemList />
  );
}

export default App;
